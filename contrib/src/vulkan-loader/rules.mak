VULKAN_LOADER_HASH :=
VULKAN_LOADER_VERSION := 1.1.107
VULKAN_LOADER_URL := https://github.com/KhronosGroup/Vulkan-Loader/archive/v$(VULKAN_LOADER_VERSION).tar.gz
VULKAN_LOADER_ARCHIVE := Vulkan-Loader-$(VULKAN_LOADER_VERSION).tar.gz

DEPS_vulkan-loader = vulkan-headers $(DEPS_vulkan-headers)

ifdef HAVE_ANDROID
PKGS_FOUND += vulkan-loader
endif

ifeq ($(TARGET_ARCH_ABI),armeabi-v7a)
PKGS_FOUND += vulkan-loader
endif

VULKAN_LOADER_CONF := \
	-DENABLE_STATIC_LOADER=ON \
	-DBUILD_TESTS=OFF \
	-DBUILD_LOADER=ON

$(TARBALLS)/$(VULKAN_LOADER_ARCHIVE):
	$(call download_pkg,$(VULKAN_LOADER_URL),vulkan-loader)

.sum-vulkan-loader: $(VULKAN_LOADER_ARCHIVE)

vulkan-loader: $(VULKAN_LOADER_ARCHIVE) .sum-vulkan-loader
	$(UNPACK)
	$(APPLY) $(SRC)/vulkan-loader/0001-WIP.patch
	#rm -rf $@ Vulkan-LoaderAndValidationLayers-sdk-1.1.73.0
	#tar xvzfo $<
	#mv Vulkan-LoaderAndValidationLayers-sdk-1.1.73.0 vulkan-loader-$(VULKAN_LOADER_VERSION)
	$(MOVE)

.vulkan-loader: vulkan-loader toolchain.cmake
	cd $< && rm -f CMakeCache.txt
	cd $< && $(HOSTVARS) $(CMAKE) --clean-first $(VULKAN_LOADER_CONF) -G"Ninja" .
	cd $< && ninja -C build install
	touch $@
